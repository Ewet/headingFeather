module.exports = Behavior({
  behaviors: [],
  properties: {
    myBehaviorProperty: {
      type: String
    }
  },
  data: {
    myBehaviorData: 555
  },
  methods: {
    myBehaviorMethod() {
      console.log(741)
    }
  }
})