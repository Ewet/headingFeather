var myBehavior = require('behavior')
Component({
  behaviors: [myBehavior],
  properties: {
    age: {
      type: Number,
      default: null
    }
  },
  data: {
    name: 'chen'
  },
  ready() {
  },
  methods: {
    onTap () {
      console.log(789)
      // myevent
      this.triggerEvent('myevent', {id: 456}, { bubbles: true })
    }
  }
})