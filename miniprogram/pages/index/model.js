import regeneratorRuntime from '../../utils/runtime.js';

const $api = require('./api.js');

const getList = async (data) => {
  let result = await $api.apiGetList(data);
  console.log(result)
  return result;
}

export {
  getList
}