const $ajax = require('./formatApi.js');

// 错误弹窗
function showErr(err) {
  const msg = $ajax.errPicker(err);
  console.log(msg)
  wx.showModal({
    showCancel: false,
    content: msg
  });
}

// 成功弹窗
function showSuc(suc) {
  wx.showModal({
    showCancel: false,
    content: suc
  });
}

export {
  showErr,
  showSuc
};