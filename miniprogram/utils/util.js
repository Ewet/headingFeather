// 检查是否授权，获取地址
const getLocation = () => {
  // 查询用户授权情况，是否授权过
  let that = this;
  wx.getSetting({
    success(res) {
      console.log(res.authSetting)
      if (res.authSetting['scope.userLocation']) {
        // 已经授权，直接获取位置
        that.locations(true);
      } else {
        // 未授权，调起授权窗口 ,已拒绝过授权，调用这个方法也不会重新调起授权窗口；
        wx.authorize({
          scope: 'scope.userLocation',
          success() {
            that.locations(true);
          },
          fail() {
            // 拒绝的操作
            errorToast('您拒绝过授权获取位置，请在设置中重新授权！')
            wx.hideToast();
            that.setData({
              noData: true
            })
            // wx.navigateBack()
            // that.locations(false);
          }
        })
      }
    }
  })
}
function locations(type) {
  const that = this;
  if (type) {
    wx.getLocation({
      type: 'wgs84', //
      altitude: false,
      success(res) {
        that.setData({
          latitude: res.latitude,
          longitude: res.longitude
        })
      }
    })
  }
  that.loadData()
}

// 直接获取本地地址
// 获取本地位置

const getLocations = () => {
  return new Promise((resovle) => {
    wx.getLocation({
      type: "wgs84",
      success(res) {
        resovle({
          success: true,
          data: {
            lng: res.longitude,
            lat: res.latitude,
          }
        })
      },
      fail(err) {
        resovle({
          success: false
        })
      }
    });
  })
}