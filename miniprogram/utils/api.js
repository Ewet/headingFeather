const app = getApp();
const $ajax = require('./formatApi.js');
let apiUrl = app.globalData.apiUrl;

const post = ({url, data}) => {
  url = apiUrl + url;
  let method = 'POST';
  return $ajax.ajaxRequest({ url, data, method });
}

const get = ({url, data}) => {
  url = apiUrl + url;
  let method = 'GET';
  return $ajax.ajaxRequest({ url, data, method });
}

const put = ({url, data}) => {
  url = apiUrl + url;
  let method = 'PUT';
  return $ajax.ajaxRequest({ url, data, method });
}


module.exports = {
  post,
  get,
  put
};
// export default {
//   post,
//   get,
//   put
// };











