
/**
 * 判断请求状态是否成功
 * 参数：http状态码
 * 返回值：[Boolen]
 */
const isHttpSuccess = (status) => {
  return status >= 200 && status < 300 || status === 304;
}

/**
 * promise请求
 * 参数：参考wx.request
 * 返回值：【promise】res
 */
 const requestPromise = (options = {}) => {
   let {
     url,
     data,
     header,
     method,
     dataType,
     responseType,
     success,
     fail,
     complete
   } = options;

   // 统一注入约定的header
   header = Object.assign({
     sessionId: sessionId
   }, options.header);
  
   return new Promise((resolve, rej) => {
     wx.request({
       url,
       data,
       header,
       method,
       dataType,
       responseType,
       success(result) {
         /**
          * result = {data, statusCode, header}
          * data = {后台的json对象}
         */
         const isSuccess = isHttpSuccess(result.statusCode);

         if (isSuccess) {  // 成功的请求状态
          //  resolve(result.data); // 这里只取data={data,success,message,statusCode}
          resolve({
            msg: result.data.message,
            success: result.data.success,
            data: result.data.data,
            statusCode: result.data.statusCode
          })
         } else {
           if (result.statusCode === 500) {
             rej({
               msg: '服务器错误',
               success: false,
               data: {},
               statusCode: 500
             });
           } else if (result.statusCode === 404) {
             rej({
               msg: '网络错误',
               success: false,
               data: {},
               statusCode: 404
             });
           } else {
             // 其它接口返回值的错误
             rej({
               msg: result.data.message,
               success: false,
               data: {},
               statusCode: result.statusCode,
               detail: result.data
             });
           }
         }
       },
       // 网络错误，服务器错误 fail只发生在系统和网络层面的失败情况，比如网络丢包、域名解析失败等等，而类似404、500(后端抛出)之类的接口状态，依旧是调用success，并体现在statusCode上。
       fail(err) {
         rej(err)
       },
       complete
     });
   });
 };


/**
 * 登录
 * 参数：undefined
 * 返回值：[promise]res
 */
let sessionId = '';
const app = getApp();
const login = () => {
  return new Promise((resolve, reject) => {
    // 微信登陆
    wx.login({
      success(res) {
        if (res.code) {
          requestPromise({
            url: `${app.globalData.apiUrl}/platform/mobile/current_lander?code=${res.code}`,
            method: "POST"
          })
          .then((result) => {
            if (result.success) {
              const { sessionId2 } = result.data;
              sessionId = 'sessionId2';
              resolve(result);
            } else {
              // 获取sessionid失败
              reject(result);
            }
          })
          .catch((err) => {
            reject(err);
          })
        } else {
          reject({
            msg: '获取code失败',
            data: res
          });
        }
      },
      fail(err) {
        rej(err);
      }
    })
  })
}

/**
 * 获取sessionId
 * 参数：undefined
 * 返回值：[promise]sessionId
 */
let loginQueue = [];
let isLoginning = false; // 是否正在登录

const getSessionId = () => {
  return new Promise((resolve, reject) => {
    // 本地sessionId丢失,重新登录
    if (!sessionId) {
      // 把所有请求放进队列
      loginQueue.push({ resolve, reject });

      if(!isLoginning) {
        isLoginning = true;
        login()
          .then(res => {
            isLoginning = false;
            // 返回sessionId
            resolve(res.data.sessionId);
            loginQueue.map((q) => q.resolve(res.data.sessionId));
            loginQueue = [];
          })
          .catch(err => {
            isLoginning = false;
            loginQueue.map((q) => q.reject(err));
            loginQueue = [];
          });
      }
    } else {
      resolve(sessionId);
    }
  })
}

/**
 * ajax高级封装
 * 参数：[Object]option = {}，参考wx.request；
 * [Boolen]keepLogin = false // 是否需要sessionId
 * 返回值：[promise]res
 */
const ajaxRequest = (options = {}, keepLogin = true) => {
  if (keepLogin) {
    return new Promise((resolve, reject) => {
      getSessionId()
      .then(res => {
        // 获取sessionId成功之后，发起请求
        requestPromise(options)
        .then(result => {
          if (result.statusCode === 401) {
            // 登录状态无效，则重新走一遍登录流程
            // 销毁本地已失效的sessionId
            sessionId = '';
            getSessionId()
            .then(r1 => {
              requestPromise(options)
              .then(r2 => {
                resolve(r2)
              })
              .catch((err) => {
                reject(err);
              });
            })
          } else {
            resolve(result)
          }
        })
        .catch(err => {
          // 请求出错
          reject(err);
        })
      })
      .catch(err => {
        // 获取sessionId失败
        reject(err);
      })
    })
  } else {
    return requestPromise(options);
  }
}

/**
 * 提炼错误信息
 * 参数：err
 * 返回值：[string]errMsg
 */
function errPicker(err) {
  if (typeof err === 'string') {
    return err;
  }

  return err.msg || err.errMsg || (err.detail && err.detail.errMsg) || '未知错误';
}



module.exports = {
  ajaxRequest,
  login,
  errPicker
};

/**
 * 1.使用promise重构request，并重置返回值
 * 
 * 2.对响应的状态码进行区分
 * 
 * 3.登录态的情况
 * 
 * 当我发起一个请求的时候，如果这个请求不需要sessionId，则直接发出；
 * 
 * 如果这个请求需要携带sessionId，就去检查现在是否有sessionId，有的话直接携带,发起请求；
 * 
 * 如果没有，自动去走登录的流程，登录成功，拿到sessionId，再去发送这个请求；
 * 
 * 如果最后请求返回结果是sessionId过期了，那么程序自动走登录的流程，然后再发起一遍。
 * 
 * 4.网络错误，服务器错误 fail的返回值：
 * {errMsg: "request:fail "}
 */