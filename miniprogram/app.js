//app.js
// const weui = require('weui')
// console.log(weui)
App({
  onLaunch: function () {
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        traceUser: true,
      })
    }
  },
  globalData: {
    apiUrl: 'http://127.0.0.1:3001'
  }
})
